import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { DataResolverService } from './core';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', redirectTo: 'tabs' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
  { path: '', loadChildren: './pages/tabs/tabs.module#TabsPageModule'},
  { path: 'newTask', loadChildren: './pages/createTask/createTask.module#CreateTaskPageModule'},

  { path: 'taskinfo', loadChildren: './pages/taskInfo/taskInfo.module#TaskInfoPageModule'},
  {
    path: 'taskinfo/:id',
    resolve: {
      special: DataResolverService
    },
    loadChildren: './pages/taskInfo/taskInfo.module#TaskInfoPageModule'
  },
  { path: 'registration', loadChildren: './pages/registration/registration.module#RegistrationPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

