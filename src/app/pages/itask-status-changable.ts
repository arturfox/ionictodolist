export interface ITaskStatusChangable {

    finishTask(id?:any): Promise<void>;
    replayTask(id?:any): Promise<void>;
    stopTask(id?:any): Promise<void>;
    startTask(id?:any): Promise<void>;
}
