import { Component, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CreateTaskModel } from '../../task-manager/models/creteate-task.model';
import { TasksService } from '../../task-manager/services/tasks-service.service';
import { StorageProvider } from '../../providers/storage-provider';
import { TasksListModelItem } from '../../task-manager/models/local/tasks-list.model.item';
import { Router } from '@angular/router';
import { BasePage } from '../Base-Page';
import { ToastController, MenuController } from '@ionic/angular';
import { AccountService } from '../../authorization/services/account-service.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { TaskCoordinates } from '../../core/models/taskcoordinates';
import { MapComponent } from '../../map/map.component';
import { PhotoService } from '../../camera/photo.service';
import { PhotoModel } from '../../camera/photo.model';

@Component({
    selector: 'app-createTask',
    templateUrl: 'createTask.page.html',
    styleUrls: ['./createTask.page.scss']
})
export class CreateTaskPage extends BasePage {

    private photoData: PhotoModel;

    public title: string;
    public description: string;
    public isStarted: boolean = false;
    public isLocationPinned: boolean = false;

    public lat: any;
    public lng: any;

    public position: string;

    @ViewChild("customMapComponent") mapElement: ElementRef<MapComponent>;

    constructor(private tasksService: TasksService,
        protected router: Router,
        protected accountService: AccountService,
        protected storage: StorageProvider,
        protected menuController: MenuController,
        toastController: ToastController,
        private geolocation: Geolocation,
        private photoService: PhotoService) {

        super(router, toastController, storage, accountService, menuController);
    }

    async ionViewWillEnter(): Promise<void> {

        await super.ionViewWillEnter();
    }

    setLongitude(lng: any): void {

        this.lng = lng;

        if (this.lng == null || this.lat == null) {
            return;
        }

        this.position = `${this.lat.toString()} ${this.lng.toString()}`;
    }

    setLatinude(lat: any): void {

        this.lat = lat;

        if (this.lat == null || this.lng == null) {
            return;
        }

        this.position = `${this.lat.toString()} ${this.lng.toString()}`;
    }

    public async ngAfterViewInit() {

        super.ngAfterViewInit();
    }

    public async addTask(form: NgForm) {

        if (!form.valid) {
            this.showMessage("Data is not valid");
            return;
        }

        var model = new CreateTaskModel();
        model.description = this.description;
        model.title = this.title;
        model.isStarted = this.isStarted;

        if (this.isLocationPinned === true && this.lat != null && this.lng != null) {
            model.coordinates = new TaskCoordinates();
            model.coordinates.lat = this.lat;
            model.coordinates.lng = this.lng;
        }

        if (this.photoData != null) {
            model.photo = this.photoData;
        }


        let token = (await this.storage.GetUser()).token;

        var result = await this.tasksService.createTask(token, model);

        if (!result.isSuceed) {

            this.showMessage(result.message);
            return;
        }

        var saveModel = new TasksListModelItem();
        saveModel.description = result.data.description;
        saveModel.title = result.data.title;
        saveModel.status = result.data.status;
        saveModel.id = result.data.id;
        saveModel.coordinates = result.data.coordinates;

        await this.storage.AddOrUpdateTask(saveModel);
        await this.router.navigate(['/home/taskstab', { isRefreshRequired: true }]);
    }


    async takePhoto(): Promise<void> {

        this.photoData = await this.photoService.takePhoto();
        if(this.photoData == null)
        {
            this.showMessage("Can't take photo");
        }

        return;
    }

}