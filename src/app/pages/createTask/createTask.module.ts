import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CreateTaskPage } from './createTask.page';
import { TaskManagerModule } from '../../task-manager/task-manager.module';
import { MapModule } from '../../map/map.module';
import { CameraModule } from '../../camera/camera.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: CreateTaskPage }]),
    TaskManagerModule,
    MapModule,
    CameraModule,
  ],
  declarations: [CreateTaskPage]
})
export class CreateTaskPageModule {}