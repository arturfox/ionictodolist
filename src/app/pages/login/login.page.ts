import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from '../../authorization/services/account-service.service';
import { StorageProvider } from '../../providers/storage-provider';
import { NgForm } from '@angular/forms';
import { BasePage } from '../Base-Page';
import { RequestLoginModel } from '../../authorization/models/api/requestloginmodel';
import { ToastController, MenuController } from '@ionic/angular';
import { AuthenticationType } from '../../core';
import { LocalUser } from '../../core/models/localuser';
import { ResponseLoginModel } from '../../authorization/models/api/responseloginmodel';

@Component({
    selector: 'app-login',
    templateUrl: 'login.page.html',
    styleUrls: ['login.page.scss']
})
export class LoginPage extends BasePage {

    public username: string;
    public password: string;

    constructor(protected router: Router,
        protected accountService: AccountService,
        protected storage: StorageProvider,
        protected menuController: MenuController,
        toastController: ToastController) {

        super(router, toastController, storage, accountService, menuController);
    }


    async ionViewWillEnter(): Promise<void> {

        await this.storage.ClearData(); 
        await super.ionViewWillEnter();
    }

    public async ngOnInit() {

        super.ngOnInit();
    }

    public async signin(){

       await this.navigateToRegistration();
    }


    public async login(form: NgForm) {

        if (!form.valid) {

            this.showMessage("Please, fill in login and password!");
            return;
        }

        var loginModel = new RequestLoginModel();
        loginModel.password = this.password;
        loginModel.username = this.username;
        loginModel.authorizationType = AuthenticationType.Default;

        var result = await this.accountService.login(loginModel);

        if (!result.isSuceed) {
            await this.showMessage(result.message);
            return;
        }

        await this.saveUserData(result.data);

        await this.navigateHome();
    }

    public async LoginG() {

        var result = await this.accountService.loginGooglePlus();

        if (result.isSuceed === false) {
            await this.showMessage(result.message);
            return;
        }

        await this.saveUserData(result.data);

        await this.navigateHome();
    }

    public async LoginFacebook() {

        var result = await this.accountService.loginFacebook();

        if (result.isSuceed === false) {
            await this.showMessage(result.message.toString());
            return;
        }

        await this.saveUserData(result.data);

        await this.navigateHome();
    }

    private async navigateHome() {

        await this.router.navigateByUrl('home');
    }

    private async navigateToRegistration() {

        await this.router.navigateByUrl('registration');
    }

    private async saveUserData(data: ResponseLoginModel): Promise<void> {

        var user = new LocalUser();
        user.token = data.accessToken;
        user.userId = data.userId;
        user.username = data.username;
        user.authenticationType = data.authType;

        await this.storage.SetUserData(user);
    }
}
