import { Component, ViewChild } from '@angular/core';
import { StorageProvider } from '../../providers/storage-provider';
import { TasksService } from '../../task-manager/services/tasks-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TasksListModel } from '../../task-manager/models/local/tasks-list.model';
import { TasksListModelItem } from '../../task-manager/models/local/tasks-list.model.item';
import { TaskListViewItem } from '../../task-manager/models/task-list-view-item';
import { TaskStatus } from '../../task-manager/enums/TaskStatus-enum';
import { IonReorderGroup, AlertController, IonSegment, ToastController, MenuController } from '@ionic/angular';
import { BasePage } from '../Base-Page';
import { DataService } from '../../core/services/data.service';
import { ITaskStatusChangable } from '../itask-status-changable';
import { AccountService } from '../../authorization/services/account-service.service';
import { UpdateTaskStatusModel } from '../../task-manager/models/api/update-task-status.model';
import { DeleteTaskModel } from '../../task-manager/models/api/delete-task.model';

@Component({
  selector: 'app-taskstab',
  templateUrl: 'taskstab.page.html',
  styleUrls: ['taskstab.page.scss']
})

export class TasksTabPage extends BasePage implements ITaskStatusChangable {
  @ViewChild(IonReorderGroup) reorderGroup: IonReorderGroup;
  @ViewChild(IonSegment) statusSegment: IonSegment;

  private STATUS_ALL = 'all';
  private STATUS_NEW = 'new';
  private STATUS_FINISHED = 'finished';

  private tasks: Array<TasksListModelItem>;
  private userName: string;

  public tasksList: Array<TaskListViewItem>;

  public statusType: any = TaskStatus;
  public editMode: boolean = false;

  private isInitialized: boolean = false;;

  constructor(
    protected router: Router,
    protected storage: StorageProvider,
    protected tasksService: TasksService,
    protected alertController: AlertController,
    private dataService: DataService,
    protected accountService: AccountService,
    protected menuController: MenuController,
    toastController: ToastController,
    activatedRoute: ActivatedRoute) {

    super(router, toastController, storage, accountService, menuController);

    this.tasks = new Array<TasksListModelItem>();
    this.tasksList = new Array<TaskListViewItem>();

    activatedRoute.params.subscribe(async (params) => {

      const isRefreshRequired = <boolean>params['isRefreshRequired'];
      if (isRefreshRequired) {

        await this.setData();
      }
    });

  }

  public async ngOnInit() {

    super.ngOnInit();

    await this.setData();
    this.isInitialized = true;
  }

  public async setData() {

    //simulate long expectation
    await super.timeout();
    //

    let localUser = await this.storage.GetUser();
    this.userName = localUser.username;
    let result = await this.tasksService.getUserTasks(localUser.token);

    if (!result.isSuceed) {
      await this.showMessage(result.message);
      return;
    }

    this.tasks.length = 0;
    result.data.tasks.forEach((obj, index) => {
      let elem = new TasksListModelItem();
      elem.id = obj.id;
      elem.description = obj.description;
      elem.title = obj.title;
      elem.status = obj.status;
      elem.coordinates = obj.coordinates;
      elem.photo = obj.photo;

      this.tasks.push(elem);
    });

    this.updateTasksViewList();

    var model = new TasksListModel();
    model.tasks = this.tasks;
    model.userId = this.userName;

    await this.storage.UpdateUsersTaks(model);

    this.dataLoaded = true;
  }

  async ionViewWillEnter(): Promise<void> {

    await super.ionViewWillEnter();
  }


  public async ionViewDidEnter() {

    if (!this.isInitialized) {
      return;
    }

    var model = await this.storage.GetUsersTasks();

    if (model == null) {
      this.dataLoaded = true;
      return;
    }

    this.tasks = model.tasks;

    this.updateTasksViewList();
    this.dataLoaded = true;
  }

  public async finishTask(id: string): Promise<void> {

    await this.changeTaskStatus(id, TaskStatus.Finished);
  }

  public async replayTask(id: string): Promise<void> {

    await this.changeTaskStatus(id, TaskStatus.Started);
  }

  public async stopTask(id: string): Promise<void> {

    await this.changeTaskStatus(id, TaskStatus.Created);
  }

  public async startTask(id: string): Promise<void> {

    await this.changeTaskStatus(id, TaskStatus.Started);
  }

  public async refreshList(refresher: any) {

      await this.setData();
      this.refreshTasks();
      refresher.detail.complete();
  }


  private async changeTaskStatus(id: string, status: TaskStatus): Promise<void> {

    let token = (await this.storage.GetUser()).token;

    let updateModel = new UpdateTaskStatusModel();
    updateModel.status = status;
    updateModel.taskId = id;

    let result = await this.tasksService.updateTaskStatus(token, updateModel);
    if (!result.isSuceed) {
      await this.showMessage(result.message);
      return;
    }

    let elem = this.tasks.find(task => task.id === id);
    if (elem == null) {
      return;
    }

    elem.status = status;

    var model = new TasksListModel();
    model.tasks = this.tasks;
    model.userId = this.userName;

    await this.storage.UpdateUsersTaks(model);
    this.refreshTasks();
  }

  //#region handlers
  public segmentChanged(ev: any) {

    this.refreshTasks();
  }

  public async createTask() {

    await this.navigateCreateTask();
  }

  public async showTaskInfo(item: TaskListViewItem) {

    let elem = this.tasks.find(x => x.id === item.id);
    await this.navigateTaskInfo(elem);
  }

  public async removeTaskSelected(item: TaskListViewItem) {

    await this.presentAlertDeleteConfirm(item.id);
  }

  public async confirmChanges() {
    this.changeEditMode();
  }

  public cancelChanges() {
    this.changeEditMode();
  }

  public doReorder(ev: any) {

    console.log('Dragged from index', ev.detail.from, 'to', ev.detail.to);

    ev.detail.complete();
  }

  public toggleReorderGroup() {
    this.changeEditMode();
  }
  //#endregion

  //#region navigation
  private async navigateCreateTask() {

    await this.router.navigateByUrl('newTask');
  }

  private async navigateTaskInfo(item: TasksListModelItem) {

    this.dataService.setData(item.id, item);
    await this.router.navigateByUrl("taskinfo/" + item.id);
  }
  //#endregion

  private changeEditMode(): void {
    this.editMode = !this.editMode;
    this.reorderGroup.disabled = !this.reorderGroup.disabled;
  }

  private async presentAlertDeleteConfirm(taskId: string) {
    const alert = await this.alertController.create({
      header: 'Archive task',
      message: 'Are you sure you want to remove task?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Ok',
          handler: async () => {
            await this.removeTask(taskId);
          },
        }
      ]
    });

    await alert.present();
  }

  private async removeTask(id: string) {

    let token = (await this.storage.GetUser()).token;

    let deleteModel = new DeleteTaskModel();
    deleteModel.id = id;

    var result = await this.tasksService.deleteTaskById(token, deleteModel);

    if (!result.isSuceed) {
      return;
    }

    this.tasks = this.tasks.filter(task => task.id !== id);
    this.refreshTasks();

    var model = new TasksListModel();
    model.tasks = this.tasks;
    model.userId = this.userName;

    await this.storage.UpdateUsersTaks(model);
  }

  private refreshTasks(): void {

    if (this.statusSegment.value === this.STATUS_ALL) {
      this.updateTasksViewList();
      return;
    }

    if (this.statusSegment.value === this.STATUS_NEW) {
      this.updateTasksViewList([TaskStatus.Started, TaskStatus.Created]);
      return;
    }

    if (this.statusSegment.value === this.STATUS_FINISHED) {
      this.updateTasksViewList([TaskStatus.Finished]);
      return;
    }

  }

  private updateTasksViewList(status?: Array<TaskStatus>): void {

    this.tasksList.length = 0;
    this.tasks.forEach((obj, index) => {

      if (status != null && !status.includes(obj.status)) {
        return;
      }

      let viewElem = new TaskListViewItem();
      viewElem.id = obj.id;
      viewElem.description = obj.description;
      viewElem.title = obj.title;
      viewElem.status = obj.status;
      viewElem.coordinates = obj.coordinates;
      viewElem.photo = obj.photo;

      this.tasksList.push(viewElem);
    });
  }
}
