import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TasksTabPage } from './taskstab.page';
import { TaskManagerModule } from '../../task-manager/task-manager.module';
import { CoreModule } from '../../core';
import { TasksService } from '../../task-manager/services/tasks-service.service';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    CoreModule,
    RouterModule.forChild([{ path: '', component: TasksTabPage }]),
    TaskManagerModule
  ],
  declarations: [TasksTabPage],
  providers:[TasksService]
})
export class TasksTabPageModule {}
