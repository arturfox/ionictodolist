import { Component, OnInit } from '@angular/core';
import { BasePage } from '../Base-Page';
import { Router } from '@angular/router';
import { AccountService } from '../../authorization/services/account-service.service';
import { StorageProvider } from '../../providers/storage-provider';
import { MenuController, ToastController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { RegisterModel } from '../../authorization/models/api/register.model';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage extends BasePage {


  public username: string;
  public password: string;
  public confirmPassword: string;

  constructor(protected router: Router,
    protected accountService: AccountService,
    protected storage: StorageProvider,
    protected menuController: MenuController,
    toastController: ToastController) {
    super(router, toastController, storage, accountService, menuController);
  }


  async signIn(form: NgForm) {

    if (!form.valid) {
      this.showMessage("Data is not valid");
      return;
    }

    let model = new RegisterModel();
    model.username = this.username;
    model.password = this.password;
    model.confirmPassword = this.confirmPassword;

    if (this.password != this.confirmPassword) {

      this.showMessage("confirm password must be same");
      return;
    }

    var result = await this.accountService.signIn(model);

    if (!result.isSuceed) {

      this.showMessage(result.message);
      return;
    }

    await this.router.navigate(['']);
  }
}
