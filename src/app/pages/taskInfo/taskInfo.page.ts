import { Component, ViewChild } from '@angular/core';
import { TasksService } from '../../task-manager/services/tasks-service.service';
import { StorageProvider } from '../../providers/storage-provider';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { BasePage } from '../Base-Page';
import { TasksListModelItem } from '../../task-manager/models/local/tasks-list.model.item';
import { TaskStatus } from '../../task-manager/enums/TaskStatus-enum';
import { IonTextarea, ToastController, MenuController } from '@ionic/angular';
import { IonInput } from '@ionic/angular'
import { EditTaskModel } from '../../task-manager/models/edittaskmodel';
import { ITaskStatusChangable } from '../itask-status-changable';
import { AccountService } from '../../authorization/services/account-service.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { MapComponent } from '../../map/map.component';
import { UpdateTaskStatusModel } from '../../task-manager/models/api/update-task-status.model';
import { Photo } from '../../core/models/photo';

@Component({
    selector: 'app-tasInfo',
    templateUrl: 'taskInfo.page.html',
    styleUrls: ['./taskInfo.page.scss']
})
export class TaskInfoPage extends BasePage implements ITaskStatusChangable {

    @ViewChild(IonTextarea) txteareaDescription: IonTextarea;
    @ViewChild(IonInput) inputTitle: IonInput;

    @ViewChild("customMapComponent") mapElement: MapComponent;

    private taskId: string;
    private task: TasksListModelItem;

    public title: string;
    public description: string;
    public status: TaskStatus;
    public statusType: any = TaskStatus;

    public editMode: boolean = false;

    public coordinates: any;

    public image: string;

    constructor(private tasksService: TasksService,
        protected accountService: AccountService,
        protected storage: StorageProvider,
        protected router: Router,
        protected activatedRoute: ActivatedRoute,
        protected menuController: MenuController,
        toastController: ToastController,
        private geolocation: Geolocation) {

        super(router, toastController, storage, accountService, menuController);
    }

    async ionViewWillEnter(): Promise<void> {

        await super.ionViewWillEnter();
    }

    public async ngOnInit() {

        super.ngOnInit();
    }

    public async ngAfterViewInit() {

        super.ngAfterViewInit();

        //#region simulated
        await super.timeout();
        //#endregion

        if (this.activatedRoute.snapshot.data['special']) {

            this.task = (this.activatedRoute.snapshot.data['special']) as TasksListModelItem;

            this.title = this.task.title;
            this.description = this.task.description;
            this.status = this.task.status;
            this.coordinates = this.task.coordinates;

            if(this.task.photo != undefined && this.task.photo != null)
            {
                this.image = this.task.photo.data;
            }
        }

        if (this.task.coordinates == null) {
            return;
        }

        const mapComp = this.mapElement;
        await mapComp.loadMap();
        mapComp.updateMarker(this.task.coordinates.lat, this.task.coordinates.lng, this.task.title, null);
    }

    //#region handlers
    public changeEditMode(): void {
        this.editMode = !this.editMode;
    }

    public async confirmChanges(): Promise<void> {

        this.description = this.txteareaDescription.value;

        let editModel = new EditTaskModel();
        editModel.id = this.task.id;
        editModel.description = this.description;

        let token = (await this.storage.GetUser()).token;

        let result = await this.tasksService.editTask(token, editModel);

        this.changeEditMode();
        if (!result.isSuceed) {
            this.title = this.task.title;
            this.description = this.task.description;
            return;
        }

        this.task.title = result.data.title;
        this.task.description = result.data.description;
        this.task.status= result.data.status;
        this.task.coordinates = result.data.coordinates;
        this.task.photo = result.data.photo;

        this.storage.AddOrUpdateTask(this.task);
        await this.router.navigate(['/home/taskstab', { isRefreshRequired: true }]);
    }

    public cancelChanges(): void {
        this.title = this.task.title;
        this.description = this.task.description;

        this.txteareaDescription.value = this.description;
        this.inputTitle.value = this.title;

        this.changeEditMode();
    }

    public async finishTask(): Promise<void> {

        await this.changeTaskStatus(TaskStatus.Finished);
    }

    public async replayTask(): Promise<void> {

        await this.changeTaskStatus(TaskStatus.Started);
    }

    public async stopTask(): Promise<void> {

        await this.changeTaskStatus(TaskStatus.Created);
    }

    public async startTask(): Promise<void> {

        await this.changeTaskStatus(TaskStatus.Started);
    }
    //#endregion

    private async changeTaskStatus(status: TaskStatus): Promise<void> {

        let token = (await this.storage.GetUser()).token;

        let updateModel = new UpdateTaskStatusModel();
        updateModel.status = status;
        updateModel.taskId = this.taskId;

        let result = await this.tasksService.updateTaskStatus(token, updateModel);
        if (!result.isSuceed) {

            await this.showMessage(result.message);
            return;
        }

        this.status = status;
        this.task.status = status;
        this.storage.AddOrUpdateTask(this.task);
    }
}