import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TaskInfoPage } from './taskInfo.page';
import { MapModule } from '../../map/map.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    MapModule,
    RouterModule.forChild([{ path: '', component: TaskInfoPage }])
  ],
  declarations: [TaskInfoPage]
})
export class TaskInfoPageModule {}