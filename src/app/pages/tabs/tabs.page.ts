import { OnInit, Component } from '@angular/core';
import { StorageProvider } from '../../providers/storage-provider';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {


  constructor(private storage: StorageProvider) { }

  public async ngOnInit() {

  }

}
