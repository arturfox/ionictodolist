import { OnInit, AfterViewInit } from '@angular/core';
import { IBasePage } from './IBase-Page-interface';
import { ToastController, MenuController } from '@ionic/angular';
import { StorageProvider } from '../providers/storage-provider';
import { AccountService } from '../authorization/services/account-service.service';
import { LocalUser } from '../core/models/localuser';
import { Router } from '@angular/router';

export class BasePage implements OnInit, AfterViewInit, IBasePage {

    private DEFAULT_TOAST_DURATION: number = 3000;
    public dataLoaded: any;

    constructor(protected router: Router,
        protected toastController: ToastController,
        protected storage: StorageProvider,
        protected accountService: AccountService,
        protected menuController: MenuController) {

    }


    protected async ionViewWillEnter() : Promise<void> {

        await this.setMenuAvailability();
    }


    public async showMessage(message: string): Promise<void> {

        const toast = await this.toastController.create({
            message: `${message}`,
            duration: this.DEFAULT_TOAST_DURATION
        });
        toast.present();
    };

    public async ngAfterViewInit() {


    }


    public async ngOnInit() {


        var isMenuEnabled = await this.setMenuAvailability();

        if (!isMenuEnabled) {

            return;
        }

        var menu_elem = (await this.menuController.getMenus())[0];

        menu_elem.addEventListener("click", async () => {

            await this.LogOut();
        });
    }

    protected async GetUser(): Promise<LocalUser> {

        return await this.storage.GetUser();
    }

    protected async LogOut() {

        var user = await this.GetUser();
        var result = await this.accountService.logOut(user.token, user.authenticationType);

        if (result.isSuceed === false) {
            await this.showMessage(result.message);
            return;
        }

        await this.storage.ClearData();
        await this.navigateToLoginPage();
    }

    private async navigateToLoginPage() {

        await this.router.navigateByUrl('');
    }

    private async setMenuAvailability():Promise<boolean>{

        var user = await this.GetUser();
        var isMenuEnabled = user != null;
        this.menuController.enable(isMenuEnabled);

        return isMenuEnabled;
    }

    //#region simulate long expectation
    protected timeout() {
        return new Promise(resolve => setTimeout(resolve, 2000));
    }

    private async sleep(fn, ...args) {
        await this.timeout();
        return fn(...args);
    }
    //#endregion
}