import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { GOOGLE_MAPS_API_KEY } from '../../environments/environment';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit, AfterViewInit {

    @Input() enableUserGeo: boolean = false;
    @Input() preloadMap: boolean = false;
    @Output() onMarkerLatitudeChanged = new EventEmitter();
    @Output() onMarkerLongitudeChanged = new EventEmitter();

    private latitude: any;
    private longitude: any;

    private isMarkerInitialized: boolean = false;
    private isMapLoaded: boolean = false;

    @ViewChild("mapElement") mapNativeElement: ElementRef;
    private googleMaps: any;
    private map: any;

    private defaultLatCenter: number = 50.437959;
    private defaultLngCenter: number = 30.578928;
    private defaultZoom: number = 15;

    constructor(private readonly geolocation: Geolocation) {


    }

    public updateMarker(latitude: any, longitude: any, label: string, title: string, draggable: boolean =  false): void {

        if (this.googleMaps == null || this.map == null || this.enableUserGeo
            || this.isMarkerInitialized) {
            return;
        }

        var latLng = new this.googleMaps.LatLng(latitude, longitude);

        var marker = new this.googleMaps.Marker({
            position: latLng,
            map: this.map,
            title: title,
            label: label,
            draggable: draggable
        });

        var mapOptions = {
            zoom: this.defaultZoom,
            mapTypeId: this.googleMaps.MapTypeId.ROADMAP,
            center: latLng
        }

        this.map.setCenter(latLng);

        this.isMarkerInitialized = true;

        this.onMarkerLatitudeChanged.emit(latitude);
        this.onMarkerLongitudeChanged.emit(longitude);

        if (draggable === false) {
            return;
        }

        this.googleMaps.event.addListener(marker, 'drag', () => {

            var markerPosition = marker.getPosition();
            this.latitude = markerPosition.lat();
            this.longitude = markerPosition.lng();

            this.onMarkerLatitudeChanged.emit(this.latitude);
            this.onMarkerLongitudeChanged.emit(this.longitude);
        });
    }

    public async ngOnInit() {

    }

    public async ngAfterViewInit() {

        if(this.preloadMap === false)
        {
            return;
        }

        await this.loadMap();
    }

    public async loadMap():Promise<void>{

        if(this.isMapLoaded === true)
        {
            return;
        }

        this.isMapLoaded = true;

        var latCenter = this.defaultLatCenter, lngCenter = this.defaultLngCenter;

        if (this.enableUserGeo === true) {
            await this.geolocation.getCurrentPosition()
                .then((res) => {

                    latCenter = res.coords.latitude;
                    lngCenter = res.coords.longitude;
                })
                .catch((error) => {

                    console.log("get geolocation error detected in MapComponent");
                });
        }
        
        this.googleMaps = await getGoogleMaps(GOOGLE_MAPS_API_KEY);

        let latLng = new this.googleMaps.LatLng(latCenter, lngCenter);

        let mapOptions = {
            center: latLng,
            zoom: this.defaultZoom,
            mapTypeId: this.googleMaps.MapTypeId.ROADMAP
        }

        const mapEle = this.mapNativeElement.nativeElement;
        this.map = new this.googleMaps.Map(mapEle, mapOptions);

        this.onMarkerLatitudeChanged.emit(latCenter);
        this.onMarkerLongitudeChanged.emit(lngCenter);

        if (this.enableUserGeo !== true) {
            return;
        }

        var marker = new this.googleMaps.Marker({
            position: latLng,
            map: this.map,
            title: "Task's Point",
            draggable: true
        });

        this.isMarkerInitialized = true;

        var markerPosition = marker.getPosition();
        this.latitude = markerPosition.lat();
        this.longitude = markerPosition.lng();

        this.googleMaps.event.addListener(marker, 'drag', () => {

            var markerPosition = marker.getPosition();
            this.latitude = markerPosition.lat();
            this.longitude = markerPosition.lng();

            this.onMarkerLatitudeChanged.emit(this.latitude);
            this.onMarkerLongitudeChanged.emit(this.longitude);
        });

    }
}

function getGoogleMaps(apiKey: string): Promise<any> {
    const win = window as any;
    const googleModule = win.google;
    if (googleModule && googleModule.maps) {
        return Promise.resolve(googleModule.maps);
    }

    return new Promise((resolve, reject) => {

        const script = document.createElement('script');
        script.src = `https://maps.googleapis.com/maps/api/js?key=${apiKey}&libraries=places`;
        script.async = true;
        script.defer = true;
        document.body.appendChild(script);
        script.onload = () => {
            const googleModule2 = win.google;
            if (googleModule2 && googleModule2.maps) {
                resolve(googleModule2.maps);
            } else {
                reject('google maps not available');
            }
        };
    });

}
