import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PhotoModel } from './photo.model';
import { File } from '@ionic-native/file/ngx';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  constructor(private camera: Camera,
              private file: File) { }


 async takePhoto():Promise<PhotoModel>{
    
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    
    let data =  await this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     let base64Image = `data:image/jpeg;base64,${imageData}`;
     let photo = new PhotoModel();

     photo.data = base64Image;
     return photo;

    }, (err) => {

      return null;

    });

    return data;
  }

}
