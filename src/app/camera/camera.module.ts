import { NgModule } from '@angular/core';
import { PhotoService } from './photo.service';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers:[PhotoService]
})
export class CameraModule { }
