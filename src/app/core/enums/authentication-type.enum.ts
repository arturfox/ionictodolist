export enum AuthenticationType {

    None = 0,
    Default = 1,
    GooglePlus = 2,
    FaceBook = 3
}
