export class ResultModel<T = void>{
    
    public data: T;
    public statusCode: number;
    public message: string;
    public isSuceed: boolean;
}