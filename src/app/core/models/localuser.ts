import { AuthenticationType } from '../enums';

export class LocalUser {
    
    public username: string;
    public userId: string;
    public authenticationType : AuthenticationType;
    public token: string;
}
