import { ResultModel } from '../models/api/result-model';

export interface IBaseService {
    
    getErrorModel<T>(route: string): ResultModel<T>;
}
