import { Injectable } from '@angular/core';
import { SERVER_URL } from '../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class HttpHelper {

    constructor(private client: HttpClient) {

    }

    public async Get(query: string, token: string = null): Promise<string> {

        var responceData = null;

        if(token !== null)
        {
            var headers = new HttpHeaders();
            headers = headers.append('Authorization', `Bearer ${token}`);

            responceData = await this.client.get(`${SERVER_URL}${query}`,{ headers: headers, responseType: "text" }).toPromise();

            return responceData;
        }

        responceData = await this.client.get(`${SERVER_URL}${query}`).toPromise();

        return responceData;
    }

    public async Post(query: string, token: string = null, content: any = null): Promise<string> {

        var headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json');      
        if(token !== null)
        {
            var headers = new HttpHeaders();
            headers = headers.append('Accept', 'application/json');
            headers = headers.append('Authorization', `Bearer ${token}`);
        }

        var responceData = null;

        responceData = await this.client.post(`${SERVER_URL}${query}`, content, { headers: headers, responseType: "text" }).toPromise();

        return responceData;
    }
}
