import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseService } from './services/baseservice';
import { HttpHelper } from './helpers';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  exports:[  ],
  providers:[ 
    BaseService,
    HttpHelper 
  ]
})
export class CoreModule { }
