import { AuthenticationType } from '../../../core';

export class ResponseLoginModel {
    
    public username: string;
    public accessToken: string;
    public userId: string;

    public authType: AuthenticationType;  
}
