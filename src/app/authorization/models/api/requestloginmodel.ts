import { RequestLoginBaseModel } from './request-login-base-model';

export class RequestLoginModel extends RequestLoginBaseModel
{
    public password: string;
}
