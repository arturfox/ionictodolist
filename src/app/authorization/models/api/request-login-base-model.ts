import { AuthenticationType } from '../../../core';

export class RequestLoginBaseModel {

    public username: string;
    public authorizationType: AuthenticationType;    
    public accessToken: string;
}
