export class AuthResponseModel {
    
    public autherizationData : any;
    public isSuceed : boolean;
    public message : string;
    public accessToken: string;
}
