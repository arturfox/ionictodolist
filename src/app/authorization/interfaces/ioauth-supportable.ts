import { AuthResponseModel } from '../models/auth-response-model';

export interface IOauthSupportable {
    
     login():Promise<AuthResponseModel>;
     logOut():Promise<boolean>;
}
