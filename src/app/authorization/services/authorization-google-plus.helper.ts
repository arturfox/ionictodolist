import { Injectable } from '@angular/core';
import { IOauthSupportable } from '../interfaces/ioauth-supportable';
import { AuthResponseModel } from '../models/auth-response-model';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { SCOPES_GOOGLE, WEB_CLIENT_ID_GOOGLE } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationGooglePlusHelper implements IOauthSupportable {

  constructor(private googlePlus: GooglePlus) {

  }

  public async login(): Promise<AuthResponseModel> {

    var authModel = new AuthResponseModel();

    var data = await this.googlePlus.login({
      'scopes': SCOPES_GOOGLE,
      'webClientId': WEB_CLIENT_ID_GOOGLE,
      'offline': false
    })
      .then(user => {
        authModel.isSuceed = true;
        authModel.message = "Authentification succeed.";
        authModel.autherizationData = user.email;
        authModel.accessToken = user.accessToken

        return authModel;

      }, err => {
        authModel.isSuceed = false;
        authModel.message = err;

        return authModel;
      });

    return data;
  }

  public async logOut(): Promise<boolean> {

    var isSuceed = await this.googlePlus.logout()
      .then(res => {
        return true;

      }, err => {
        return false;

      });

      return isSuceed;
  };
  
}
