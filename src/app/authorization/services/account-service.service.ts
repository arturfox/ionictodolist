import { Injectable } from '@angular/core';
import { HttpHelper, AuthenticationType } from '../../core';
import { ResultModel } from '../../core';
import { BaseService } from '../../core';
import { ResponseLoginModel } from '../models/api/responseloginmodel';
import { RequestLoginBaseModel } from '../models/api/request-login-base-model';
import { AuthorizationGooglePlusHelper } from './authorization-google-plus.helper';
import { AuthorizationFacebookHelper } from './authorization-facebook.helper';
import { IOauthSupportable } from '../interfaces/ioauth-supportable';
import { RegisterModel } from '../models/api/register.model';

//todo: add authomatic auth logout when login to api is failed
@Injectable({ providedIn: 'root' })
export class AccountService extends BaseService {

    constructor(private httphelper: HttpHelper,
        private googlePlusHelper: AuthorizationGooglePlusHelper,
        private facebookHelper: AuthorizationFacebookHelper
    ) {

        super();
    }


    public async loginGooglePlus(): Promise<ResultModel<ResponseLoginModel>> {

        var authorizationResult = await this.authorizateByType(AuthenticationType.GooglePlus);

        return authorizationResult;
    }

    public async loginFacebook(): Promise<ResultModel<ResponseLoginModel>> {

        var authorizationResult = await this.authorizateByType(AuthenticationType.FaceBook);

        return authorizationResult;
    }


    public async login(model: RequestLoginBaseModel): Promise<ResultModel<ResponseLoginModel>> {

        var route = "account/login/";

        try {

            let data = await this.httphelper.Post(`${route}`, null, model);
            let result: ResultModel<ResponseLoginModel> = JSON.parse(data);

            return result;
        }
        catch (e) {
            var errorModel = this.getErrorModel<ResponseLoginModel>(`${route}`);
            return errorModel;
        }
    }

    public async signIn(model: RegisterModel): Promise<ResultModel> {

        var route = "account/register/";

        try {

            let data = await this.httphelper.Post(`${route}`, null, model);
            let result: ResultModel<void> = JSON.parse(data);

            return result;
        }
        catch (e) {
            var errorModel = this.getErrorModel<void>(`${route}`);
            return errorModel;
        }
    }


    public async logOut(token: string, type: AuthenticationType): Promise<ResultModel> {

        var helper = this.getAuthorizationHelperByType(type);

        if (helper !== null) {
            await helper.logOut();
        }

        var route = "account/logout";

        try {

            let data = await this.httphelper.Get(`${route}`, token);
            let result: ResultModel<void> = JSON.parse(data);

            return result;
        }
        catch (e) {
            
            var errorModel = this.getErrorModel<void>(`${route}`);
            return errorModel;
        }
    }

    private async authorizateByType(type: AuthenticationType): Promise<ResultModel<ResponseLoginModel>> {

        var authorizationResult = new ResultModel<ResponseLoginModel>();
        var helper = this.getAuthorizationHelperByType(type);

        var loginData = await helper.login();
        if (loginData.isSuceed === false) {
            authorizationResult.isSuceed = false;
            authorizationResult.message = loginData.message;

            return authorizationResult;
        }

        var requestData = new RequestLoginBaseModel();
        requestData.username = loginData.autherizationData;
        requestData.authorizationType = type;
        requestData.accessToken = loginData.accessToken;

        var responceLogin = await this.login(requestData);
        return responceLogin;
    }

    private getAuthorizationHelperByType(type: AuthenticationType): IOauthSupportable {

        if (type === AuthenticationType.GooglePlus) {
            return this.googlePlusHelper;
        }

        if (type === AuthenticationType.FaceBook) {
            return this.facebookHelper;
        }

        return null;
    }
}