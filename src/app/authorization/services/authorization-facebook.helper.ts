import { Injectable } from '@angular/core';
import { AuthResponseModel } from '../models/auth-response-model';
import { IOauthSupportable } from '../interfaces/ioauth-supportable';
import { Facebook } from '@ionic-native/facebook/ngx';
import { SCOPES_FACEBOOK } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationFacebookHelper implements IOauthSupportable {

  constructor(private facebook: Facebook) { }

  public async login(): Promise<AuthResponseModel> {

    var authModel = new AuthResponseModel();

    var data = await this.facebook.login(SCOPES_FACEBOOK)
      .then(response => {

        let userId = response.authResponse.userID;
        authModel.autherizationData = userId;

        var authData = this.facebook.api("/me?fields=name,email", SCOPES_FACEBOOK)
          .then(user => {

            authModel.isSuceed = true;
            authModel.message = "Authentification succeed.";

            if (user.email !== null) {
              
              user.autherizationData = user.email;
              return authModel;
            }

            return authModel;
          });

        return authData;
      },
        error => {

          authModel.isSuceed = false;
          authModel.message = error;

          return authModel;
        });

    return data;
  }

  public async logOut(): Promise<boolean> {

    var isSuceed = await this.facebook.logout()
      .then(res => {
        return true;

      }, err => {
        return false;
      });

    return isSuceed;
  };
}
