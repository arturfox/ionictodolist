import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestLoginModel } from './models/api/requestloginmodel';
import { AccountService } from './services/account-service.service';
import { CoreModule } from '../core';
import { ResponseLoginModel } from './models/api/responseloginmodel';
import { RequestLoginBaseModel } from './models/api/request-login-base-model';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CoreModule,
    GooglePlus,
    Facebook
  ],
  exports: [
    RequestLoginModel,
    RequestLoginBaseModel,
    ResponseLoginModel
  ],
  providers: [
    AccountService
  ]
})
export class AuthorizationModule { }
