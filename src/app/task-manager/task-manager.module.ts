import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core';
import { TasksService } from './services/tasks-service.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CoreModule
  ],
  providers: [
    TasksService
  ]
})
export class TaskManagerModule { }
