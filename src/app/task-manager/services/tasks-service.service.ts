import { Injectable } from '@angular/core';
import { GetTasksModel } from '../models/api/get-tasks-model';
import { GetTasksModelItem } from '../models/api/get-tasks-model.item';
import { CreateTaskModel } from '../models/creteate-task.model';
import { ResultModel, HttpHelper, BaseService } from '../../core';
import { EditTaskModel } from '../models/edittaskmodel';
import { UpdateTaskStatusModel } from '../models/api/update-task-status.model';
import { DeleteTaskModel } from '../models/api/delete-task.model';

@Injectable({
    providedIn: 'root'
})
export class TasksService extends BaseService {

    constructor(private httphelper: HttpHelper) {

        super();
    }

    public async getUserTasks(token: string): Promise<ResultModel<GetTasksModel>> {

        var route = "tasks/getTasks/";

        try {

            let data = await this.httphelper.Get(`${route}`, token);
            let result: ResultModel<GetTasksModel> = JSON.parse(data);

            return result;
        }
        catch (e) {

            var errorModel = this.getErrorModel<GetTasksModel>(`${route}`);
            return errorModel;
        }
    }

    public async createTask(token: string, model: CreateTaskModel): Promise<ResultModel<GetTasksModelItem>> {
        
        var route = "tasks/addTask";

        try {

            let data = await this.httphelper.Post(`${route}`, token, model);
            let result: ResultModel<GetTasksModelItem> = JSON.parse(data);

            return result;
        }
        catch (e) {

            console.log(e.toString());
            var errorModel = this.getErrorModel<GetTasksModelItem>(`${route}`);
            return errorModel;
        }
    }


    public async updateTaskStatus(token: string, updateModel: UpdateTaskStatusModel): Promise<ResultModel<GetTasksModelItem>> {

        var route = "tasks/updateTaskStatus/";

        try {

            let data = await this.httphelper.Post(`${route}`, token, updateModel);
            let result: ResultModel<GetTasksModelItem> = JSON.parse(data);

            return result;
        }
        catch (e) {

            var errorModel = this.getErrorModel<GetTasksModelItem>(`${route}`);
            return errorModel;
        }
    }

    public async deleteTaskById(token:string, deleteModel: DeleteTaskModel): Promise<ResultModel> {

        var route = "tasks/deleteTask";

        try {

            let data = await this.httphelper.Post(`${route}`, token, deleteModel);
            let result: ResultModel = JSON.parse(data);

            return result;
        }
        catch (e) {

            var errorModel = this.getErrorModel<void>(`${route}`);
            return errorModel;
        }
    }

    public async editTask(token: string,editmodel: EditTaskModel): Promise<ResultModel<GetTasksModelItem>> {

        var route = "tasks/editTask";

        try {

            let data = await this.httphelper.Post(`${route}`, token, editmodel);
            let result: ResultModel<GetTasksModelItem> = JSON.parse(data);

            return result;
        }
        catch (e) {
            
            var errorModel = this.getErrorModel<GetTasksModelItem>(`${route}`);
            return errorModel;
        }
    }
}