import { TaskStatus } from '../../enums/TaskStatus-enum';
import { TaskCoordinates } from '../../../core/models/taskcoordinates';
import { Photo } from '../../../core/models/photo';

export class TasksListModelItem {

    public title: string;
    public description: string;
    public status: TaskStatus;

    public id: string;

    public coordinates: TaskCoordinates;

    public photo: Photo;

    constructor() {

    }
}