import { TasksListModelItem } from './tasks-list.model.item';

export class TasksListModel {

    public userId: string;
    public tasks: Array<TasksListModelItem>;

    constructor() {

        this.tasks=new Array<TasksListModelItem>();
    }
}