import { TaskCoordinates } from '../../core/models/taskcoordinates';
import { PhotoModel } from '../../camera/photo.model';

export class CreateTaskModel{

    public title:string;
    public description:string;
    public isStarted:  boolean;

    public coordinates: TaskCoordinates;
    public photo: PhotoModel;

    constructor()
    {

    }  
}