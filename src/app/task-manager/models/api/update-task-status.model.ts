import { TaskStatus } from '../../enums/TaskStatus-enum';

export class UpdateTaskStatusModel {

    public taskId : string;
    public status: TaskStatus;
}
