import { TaskStatus } from '../enums/TaskStatus-enum';
import { TaskCoordinates } from '../../core/models/taskcoordinates';
import { Photo } from '../../core/models/photo';

export class TaskListViewItem {

    public title: string;
    public description: string;
    public status: TaskStatus;
    public id: string;

    public isCreated = (this.status == TaskStatus.Created);
    public isStarted = (this.status == TaskStatus.Started)
    public isFinished = (this.status == TaskStatus.Finished);

    public coordinates: TaskCoordinates;
    public photo: Photo;

    constructor() {

    }
}