import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { TasksListModel } from '../task-manager/models/local/tasks-list.model';
import { TasksListModelItem } from '../task-manager/models/local/tasks-list.model.item';
import { LocalUser } from '../core/models/localuser';

@Injectable()
export class StorageProvider {

    private USER_DATA_KEY = 'user';
    private TASKS_LIST_KEY = 'taskslist';

    constructor(private storage: Storage) {

    }

    public async SetUserData(user: LocalUser) {

        await this.storage.set(this.USER_DATA_KEY, user);
    }

    public async GetUser(): Promise<LocalUser> {

        var userData;
        await this.storage.get(this.USER_DATA_KEY).then((value) => { userData = value });

        return userData;
    }

    public async UpdateUsersTaks(model: TasksListModel) {

        await this.storage.set(this.TASKS_LIST_KEY, model);
    }

    public async GetUsersTasks(): Promise<TasksListModel> {

        var model;
        await this.storage.get(this.TASKS_LIST_KEY).then((value) => { model = value });

        return model;
    }

    public async AddOrUpdateTask(model: TasksListModelItem) {

        var userTasks = await this.GetUsersTasks();

        if(userTasks === null)
        {
            userTasks = new TasksListModel();
        }
        
        userTasks.tasks=userTasks.tasks.filter(task=>task.id !== model.id);
        userTasks.tasks.push(model);
        await this.UpdateUsersTaks(userTasks);
        return;     
    }

    public async ClearData(){

        await this.storage.clear();

    }
}